FROM debian:bullseye as builder

ARG prefix=/opt/dev_toolchain_13

ARG gcc_version=13.2.0

RUN apt-get -qq update \
    && apt-get -qq install -y --no-install-recommends --no-install-suggests -o=Dpkg::Use-Pty=0 \
       g++-multilib \
       gcc \
       curl \
       wget \
       zlib1g-dev \
       ca-certificates \
       autoconf-archive \
       binutils \
       libgmp-dev \
       libmpfr-dev \
       libmpc-dev \
       nasm \
       dh-autoreconf \
       libffi-dev \
       libssl-dev \
       pkg-config \
       make

RUN cd /tmp && wget -q --no-check-certificate http://mirror.koddos.net/gcc/releases/gcc-${gcc_version}/gcc-${gcc_version}.tar.xz \
    && tar Jxf gcc-${gcc_version}.tar.xz &&\
    cd gcc-${gcc_version} \
    && ./configure \
                   --build=x86_64-linux-gnu \
                   --disable-bootstrap \
                   --disable-multilib \
                   --disable-nls \
                   --enable-languages=c,c++,fortran \
                   --disable-werror \
                   --without-isl \
                   --prefix=${prefix} \
                   --enable-linker-build-id \
    && make -s -j$(nproc) \
    && make install-strip \
    && cd / && rm -rf /tmp/*

RUN apt-get -qq install -y git

ARG cmake_version=3.28.1
RUN cd /tmp && git clone -b v${cmake_version} --depth=1 https://gitlab.kitware.com/cmake/cmake.git &&\
    cd cmake &&\
    ./bootstrap --parallel=$(nproc) --prefix=${prefix} --verbose -- \
        -DCMAKE_BUILD_TYPE=RELEASE \
        &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

FROM scratch as storage
COPY --from=builder ${prefix} ${prefix}
